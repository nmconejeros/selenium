package tests;

import java.util.HashMap;

import org.testng.annotations.Test;

import config.BaseConfig;
import pages.LandingPage;
import pages.ShopPage;
import utils.Utils;

public class Tests extends BaseConfig {

	// Checks the logIn functionality
	@Test(groups = { "logIn" }, dataProviderClass = Utils.class, dataProvider = "logIn")
	public void loginTest(HashMap<String, String> hashMap) {
		LandingPage landingPage = new LandingPage(driver);
		landingPage.closePopUps();
		landingPage.login(hashMap.get("email"), hashMap.get("password"), hashMap.get("alertMsg"));
	};

	// Checks if the links of the main drawer are broken/not assigned
	@Test(groups = { "menu" })
	public void menuTest() {
		LandingPage landingPage = new LandingPage(driver);
		landingPage.closePopUps();
		landingPage.checkLinks();
	};

	@Test(groups = "cart")
	public void cartTest() {
		ShopPage shopPage = new ShopPage(driver);
		shopPage.closePopUps();
		shopPage.addToCart();
	};

}
