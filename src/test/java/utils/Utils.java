package utils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Map;
import java.util.HashMap;

import org.testng.annotations.DataProvider;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.File;

public class Utils {

	@DataProvider(name = "logIn")
	public static Object[][] dataProvider() {
		
		//returns the function readJson
		return readJson("src/main/resources/data/Data.json", "LogIn");
	}

	public static Object[][] readJson(String filename, String jsonObj) {
		//Initializing the file object and the Json
		File file = new File(filename);
		JsonElement jsonElement = null;
		
		//Check for errors while reading the the Json file
		try {
			jsonElement = JsonParser.parseReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		//checks if the data is read correctly
		assert jsonElement != null;
		//Saves the entire data of the json file
		JsonObject jsonObj1 = jsonElement.getAsJsonObject();
		
		
		JsonArray jsonArray = jsonObj1.get(jsonObj).getAsJsonArray();
		
		//New object to save the desired data
		Object[][] testData = new Object[jsonArray.size()][1];
		
		//Loops to read every line of the Json file
		for (int i = 0; i < jsonArray.size(); i++) {
			
			//New JsonObject to get a specific set of data
			JsonObject jsonObj2 = jsonArray.get(i).getAsJsonObject();
			Map<Object, Object> map = new HashMap<>();
			
			//Loops to read all the keys of the Json object
			for (String key : jsonObj2.keySet()) {
				
				//Gets the keys and their values
				String value = jsonObj2.get(key).getAsString();
				
				//Assigns the data
				map.put(key, value);
			}
			
			//Saves the data
			testData[i][0] = map;
		}
		return testData;
	}
}
