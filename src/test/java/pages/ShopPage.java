package pages;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;

public class ShopPage {

	private WebDriver driver;

	@FindBy(xpath = "//div[@id=\"testId-searchResults-products\"]/div")
	private List<WebElement> items;

	@FindBy(xpath = "//button[contains(@id, 'testId-Pod-action-')]")
	private List<WebElement> addToCart;

	@FindBy(xpath = "//button[@class=\"jsx-1941891310\"]")
	private WebElement modalExitButton;

	@FindBy(xpath = "//div[@class=\"dy-lb-close\"]")
	private WebElement landingModal;

	public ShopPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	};

	public void closePopUps() {

		try {
			// Closing Modal
			landingModal.click();
		} catch (Exception e) {
			System.out.println(e);
		}

		// Closing PopUp
		WebElement shadowHost = driver.findElement(By.xpath("//div[@class=\"airship-html-prompt-shadow\"]"));
		SearchContext shadowRoot = shadowHost.getShadowRoot();
		shadowRoot.findElement(By.cssSelector("button.airship-btn.airship-btn-deny")).click();
	};

	public void addToCart() {

		int itemsToAdd = 10;

		for (int index = 0; index < itemsToAdd; index += 1) {

			// Scroll into view
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", items.get(index));

			// Hover to an item to enable the 'add to cart' option
			new Actions(driver).moveToElement(items.get(index)).build().perform();

			// Adds the item
			addToCart.get(index).click();

			new WebDriverWait(driver, Duration.ofSeconds(6))
					.until(ExpectedConditions.elementToBeClickable(modalExitButton));
			modalExitButton.click();

		}

	}
}
