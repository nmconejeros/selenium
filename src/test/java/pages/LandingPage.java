package pages;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LandingPage {

	private WebDriver driver;

	@FindBy(id = "testId-cc-login-form-email-input")
	private WebElement email;
	@FindBy(id = "testId-cc-login-form-password-input")
	private WebElement password;
	@FindBy(xpath = "//*[@id=\"testId-cc-login-form-submit\"]")
	private WebElement loginSubmitButton;
	@FindBy(xpath = "//div[@class=\"Popover-module_popover-container__3qpkj Popover-module_bottom-caret-notlogin__19OPO\"]")
	private WebElement dropDown;
	@FindBy(xpath = "//div[@class=\"dy-lb-close\"]")
	private WebElement modalExitButton;
	@FindBy(id = "testId-loggedout-item-0")
	private WebElement dropDownLoginOption;
	@FindBy(id = "testId-HamburgerBtn-toggle")
	private WebElement sideBarToggle;
	@FindBy(xpath = "//div[@class=\"TaxonomyDesktop-module_TaxonomyDesktop__38GRl sidebar-open-enter-done\"]")
	private WebElement sideBar;
	@FindBy(xpath = "//div[@class=\"TaxonomyDesktop-module_categoryWrapper__3YBaF\"]")
	List<WebElement> menuOptions;

	@FindBy(xpath = "//ul[@class=\"SecondLevelCategories-module_secondLevelCategory__3SPXi\"]/li[@class=\"SecondLevelCategories-module_secondLevelCategoryTitle__13Ukq\"]/a")
	List<WebElement> menuOptionsSecondLevel;

	@FindBy(xpath = "//div[@class=\"alert-module_falw-alert-container__KIuRf alert-module_falw-alert-type-warning__1qffA\"]")
	private WebElement alert;

	public LandingPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	};

	public void closePopUps() {

		// Closing Modal
		modalExitButton.click();

		// Closing PopUp
		WebElement shadowHost = driver.findElement(By.xpath("//div[@class=\"airship-html-prompt-shadow\"]"));
		SearchContext shadowRoot = shadowHost.getShadowRoot();
		shadowRoot.findElement(By.cssSelector("button.airship-btn.airship-btn-deny")).click();
	};

	public void login(String username, String passwd, String alertMessage) {

		// Open DropDown
		new Actions(driver).moveToElement(dropDown).build().perform();

		// Open Login modal
		dropDownLoginOption.click();

		// Entering Data
		email.sendKeys(username);
		password.sendKeys(passwd);
		loginSubmitButton.click();

		// checking the alert message
		new WebDriverWait(driver, Duration.ofSeconds(2)).until(ExpectedConditions.visibilityOf(alert));
		boolean alertDisplayed = alert.isDisplayed();
		if (alertDisplayed) {
			alert.getText().equals(alertMessage);
		} else {
			System.out.println("Alert missing");
		}
	};

	public void checkLinks() {

		// Open Drawer
		sideBarToggle.click();
		for (WebElement MenuOptions : menuOptions) {

			new WebDriverWait(driver, Duration.ofSeconds(2)).until(ExpectedConditions.visibilityOf(sideBar));

			new Actions(driver).moveToElement(MenuOptions).build().perform();

			for (WebElement MenuOptionsSecondLevel : menuOptionsSecondLevel) {

				// Gets the URL form the title
				String URL = MenuOptionsSecondLevel.getAttribute("href");

				// Checks if the URL exists
				if (URL == null || URL.isEmpty()) {
					System.out.println("Empty URL or not assigned");
					//continue;
				}

				// Checks if there is any problem with the links provided
				// Will send a request to every link and check its response
				try {

					// Initializing the variables which will contain the connection and the response
					// code.
					HttpURLConnection huc = null;
					int respCode = 200;

					// Connects to the URL provided
					huc = (HttpURLConnection) (new URL(URL).openConnection());

					// Set to only receive the headers
					huc.setRequestMethod("HEAD");

					// Opens the connection
					huc.connect();

					// Gets the response code
					respCode = huc.getResponseCode();

					if (respCode >= 400) {
						System.out.println(
								URL + " \nEither a broken link or some elements couldn't be loaded/are broken \n\n");
					} else {
						System.out.println(URL + " \nis a valid link \n\n");
					}

				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();

				}
			}
		}
	};
}
