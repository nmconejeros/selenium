package config;

import java.time.Duration;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseConfig {

	public static String browser = "Chrome";
	public static WebDriver driver;

	@BeforeMethod(groups = { "logIn", "menu" })
	public void setup() {
		if (browser.equals("Firefox")) {
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
		} else if (browser.equals("Chrome")) {
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
		}

		driver.get("https://www.falabella.com/falabella-cl");
		driver.manage().window().setSize(new Dimension(1920, 1080));
	};

	@BeforeMethod(groups = { "cart" })
	public void cartSetup() {
		if (browser.equals("Firefox")) {
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
		} else if (browser.equals("Chrome")) {
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(7));
		}

		driver.get("https://www.falabella.com/falabella-cl/category/cat1012/TV-y-Video");
		driver.manage().window().setSize(new Dimension(1920, 1080));
	};

	@AfterMethod(groups = { "logIn", "menu", "cart" })
	public void tearDown() {
		driver.close();
	}

}
